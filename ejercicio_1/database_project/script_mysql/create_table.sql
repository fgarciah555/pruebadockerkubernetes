CREATE TABLE logVisitas (
    id INT(3) PRIMARY KEY AUTO_INCREMENT,
    ipAddress varchar(250) NOT NULL,
    fechaVisita varchar(8) NOT NULL
);