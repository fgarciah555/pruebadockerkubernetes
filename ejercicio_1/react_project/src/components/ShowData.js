import axios from 'axios'
import React, { useState, useEffect } from 'react'

export default function ShowData() {

    const [data,setData] = useState([])

    const instance = axios.create({
        baseURL: `http://${process.env.REACT_APP_BACKHOSTNAME}:${process.env.REACT_APP_NODE_LOCAL_PORT}`,
    })

    const getVisitas = () => {
        console.log('function getVisitas')
        instance.get("/visitas/getVisitas")
        .then((result) => {
            console.log(result)
            if(Array.isArray(result.data)){
                setData(result.data) 
            }else{
                setData([]);
            }
        })
    }

    const insertVisita = () => {
        console.log('function insertVisita') 
        instance.post("/visitas/registraVisita")
        .then((result) => { 
            console.log(result)
            getVisitas()
        })
    }

    const IngresaVisita = () => {
        return (<><button onClick={insertVisita}>Agrega Registro</button></>)
    }

    const MuestraVisitas = () => {
        return (
                <>
                    <table>
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Ip Adress</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                data.map((row) => {
                                    return(
                                        <tr key={row.id}>
                                            <td>{row.id}</td>
                                            <td>{row.ipAddress}</td>
                                            <td>{row.fechaVisita}</td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </>
        )
    }
    
    useEffect(() => {
        getVisitas()
        console.log('Hostname: ',process.env.REACT_APP_BACKHOSTNAME);
        console.log('LocalPort: ',process.env.REACT_APP_NODE_LOCAL_PORT);
    }, [])

    return (
            <>
                <h1>Front</h1>
                <div>
                    <IngresaVisita />
                </div>
                <div>
                    <MuestraVisitas />
                </div>
            </>
            )
}