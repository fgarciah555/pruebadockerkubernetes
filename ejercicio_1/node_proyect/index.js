const express = require("express")
const app = express()
const cors = require("cors")
const visitasController = require("./src/Controllers/visitas")
require("dotenv").config();

//Setea Puerto//
const PORT = process.env.NODE_DOCKER_PORT || 3001

app.use(cors({ origin: '*' }))
app.get("/", (req, res) => { res.send("Hola Naha") })
app.get("/visitas/getVisitas/", visitasController.getVisitas)
app.post("/visitas/registraVisita/", visitasController.insertVisita)

app.listen(PORT, () => {
    console.log(`El servicio esta corriendo en el puerto ${PORT}`)
})