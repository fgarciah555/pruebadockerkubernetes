const mysql = require("mysql2")
const { promisify } = require("util")

const conexion = {
    connectionLimit : 10,
    host            : process.env.DB_HOST || 'localhost',
    user            : process.env.USER_DB || 'remoteAdmin',
    password        : process.env.DB_PASSWORD || 'Pst3315Z',
    database        : process.env.DB_NAME || 'Visitas'
}

console.log(conexion)

const conn = mysql.createPool(conexion)

conn.getConnection((err,connection) => {
    if(err){
        if(err.code === 'PROTOCOL_CONNECTION_CLOSE'){
            console.error('La Conexion con la base de datos se cerro');
        }
        if(err.code === 'ER_CON_COUNT_ERROR'){
            console.error('La base de datos tiene muchas conexiones');
        }
        if(err.code === 'ECONNREFUSED'){
            console.error('La Conexion fue rechazada');
        }
        console.log(err)
        return;
    }
    if(connection){
        console.log('Conexion a base de datos Exitosa');
    }
    return
})

conn.query = promisify(conn.query)
conn.format = mysql.format();

module.exports = { conn }