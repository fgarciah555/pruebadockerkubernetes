const db = require("../Models/genericModel")

const getVisitas = async (req,res) => {
    console.log('function getVisitas')
    db.setSelect("*");
    db.setTabla("logVisitas");
    let result = await db.genericSelectData()
    db.limpiaRegistros()
    console.log(result)
    res.send(result)
}

const insertVisita = async(req,res) => {
    console.log('function insertVisita')
    let ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
    let fecha = new Date();
    let date = ("0" + fecha.getDate()).slice(-2)
    let month = ("0" + (fecha.getMonth() + 1)).slice(-2)
    let year = fecha.getFullYear()
    let finalDate = year+month+date
    db.setObject({ ipAddress: ip, fechaVisita: finalDate })
    db.setTabla("logVisitas")
    let result = await db.genericInsert()
    db.limpiaRegistros()
    console.log(result)
    res.send(result)
}

module.exports = { getVisitas, insertVisita }