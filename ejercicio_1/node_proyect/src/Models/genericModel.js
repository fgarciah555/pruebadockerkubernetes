var { conn } = require("../Database/database");
var mysql = require("mysql2");
var tabla = '';
var select = '';
var orderby = undefined;
var pagination = null;
var where = [];
var whereOr = [];
var join = [];
var comparadores = [];
var object = {};

function setSelect(data){
    if(select != ''){
        select = select + ',' + data;
    }else{
        select = data;
    }
};

function setId(data){
    id = data;
}

function setTabla(data){
    tabla = data;
};

//JOIN,TABLA,COMPARADOR
//(INNER,LEFT,RIGHT) JOIN (TABLA), ON (COMPARADOR)
function setJoin(data){
    join.push(data);
};


//SELECTOR,COMPARADOR,WHERE
//Columna,Comparador Logico, Valor de Busqueda
function setWhere(data){
    where.push(data);
};

//SELECTOR,COMPARADOR,WHERE
//Columna,Comparador Logico, Valor de Busqueda
function setWhereOr(data){
    whereOr.push(data);
}

function setOrderBy(data){
    orderby = data;
};

function setPagination(data){
    pagination = data;
};

function setObject(data){
    object = data;
}

async function genericSelect(){
    var query = consultaSelect();
    query = generaJoins(query);
    query = consultaWheres(query);
    query = generaPaginations(query);
    query = generaOrderBy(query);
    query = mysql.format(query,tabla);
    try {
        var data = await conn.query(query);
        var status = true;
    }catch(e){
        var data = e.sqlMessage;
        var status = false;
    }
    var result = {'status' : status, 'data' : data};
    return result;
};

async function genericSelectData(){
    var query = consultaSelect();
    comparadores.push(tabla);
    query = generaJoins(query);
    query = consultaWheres(query);
    query = await generaPaginations(query);
    query = generaOrderBy(query);
    try {
        var data = await conn.query(query);
    }catch(e){
        var data = e.sqlMessage;
    }
    return data;
};

async function genericSelectOne(){
    var query = consultaSelect();
    query = generaJoins(query);
    query = consultaWheres(query);
    query = generaPaginations(query);
    query = generaOrderBy(query);
    try {
        var data = await conn.query(query);
    }catch(e){
        var data = e.sqlMessage;
    }
    var result = data[0];
    return result;
};

async function genericSelectCount(){
    var query = 'SELECT COUNT(*) as count FROM ?? ';
    comparadores.push(tabla);
    query = generaJoins(query);
    query = consultaWheres(query);
    query = generaOrderBy(query);
    try {
        var conne = await conn.query(query);
        var status = true;
    }catch(e){
        var data = e.sqlMessage;
        var status = false;
    }
    if(status){
        data = conne[0].count;
    }
    var result = {'status' : status, 'data' : data};
    return result;
};

function generaJoins(query){
    if(join.length > 0){
        join.forEach(data => {
            limpiaComparadores();
            query = query + (data.join + ' JOIN ?? ON ' + data.comparador);
            comparadores.push(data.tabla);
            query = mysql.format(query,comparadores);
        });
    };
    return query;
};

function consultaSelect(){
    var query = 'SELECT '+select+' FROM ?? ';
    query = mysql.format(query,tabla);
    return query;        
}

function consultaWheres(query){
    if(where.length > 0 && whereOr.length > 0){
        query = query + 'WHERE ';
        query = generaWheres(query);
        query = query + 'AND ';
        query = generaWheresOr(query);

    }else{
        if(where.length > 0 || whereOr.length > 0){
            query = query + 'WHERE ';
            query = generaWheres(query);
            query = generaWheresOr(query);
        }
    }
    return query;
}

function generaWheres(query){
    var counter = 0;
    if(where.length > 0){
        where.forEach(data => {
            limpiaComparadores();
            if(counter == 0){
                query = query + (' ?? '+data.comparador+' ? ');
            }else{
                query = query + ('AND ?? '+data.comparador+' ? ');
            }
            comparadores.push(data.selector,data.where);
            query = mysql.format(query,comparadores);
            counter++;
        });
    };
    return query;
};

function generaWheresOr(query){
    var counter = 0;
    if(whereOr.length > 0){
        query = query + ' (';
        whereOr.forEach(data => {
            limpiaComparadores();
            if(counter == 0){
                query = query + ('?? '+data.comparador+' ? ');
            }else{
                query = query + ('OR ?? '+data.comparador+' ?');
            }
            comparadores.push(data.selector,data.where);
            query = mysql.format(query,comparadores);
            counter++;
        });
        query = query + ')';
    };
    return query;
};

function generaOrderBy(query){
    if(orderby != undefined){
        query = query + 'ORDER BY ' + orderby.selector + ' ' + orderby.orderBy;
    }
    return query;
};

function generaPaginations(query){
    if(pagination != undefined){
        limpiaComparadores();
        query = query + 'limit ? OFFSET ?';
        comparadores.push(pagination.limit,pagination.start);
        query = mysql.format(query,comparadores);
    }
    return query;
};


function limpiaRegistros(){
    tabla = '';
    select = '';
    id = '';
    orderby = undefined;
    pagination = undefined;
    where = [];
    whereOr = [];
    join = [];
    comparadores = [];
    object = {};
};

function limpiaComparadores(){
    comparadores = [];
}

async function genericInsert(){
    var query = 'INSERT INTO ?? set ?';
    var table = [tabla,object];
    query = mysql.format(query,table);
    try {
        var data = await conn.query(query);
        data = data.insertId;
        var status = true;
    }catch(e){
        var data = e.sqlMessage;
        var status = false;
    }
    var result = {'status' : status, 'data' : data};
    return result;
};

async function genericUpdate(){
    var query = 'UPDATE ?? set ? ';
    comparadores.push(tabla,object);
    query = mysql.format(query,comparadores);
    query = consultaWheres(query);
    try {
        var data = await conn.query(query);
        data = data.affectedRows;
        var status = true;
    }catch(e){
        var data = e.sqlMessage;
        var status = false;
    }
    var result = { 'status' : status , 'data' : data };
    return result;
};

async function genericDelete(){
    var query = 'DELETE FROM ?? ';
    comparadores.push(tabla);
    query = mysql.format(query,comparadores);
    query = consultaWheres(query);
    try {
        var data = await conn.query(query);
        data = data.affectedRows;
        var status = true;
    }catch(e){
        var data = e.sqlMessage;
        var status = false;
    }
    var result = { 'status' : status , 'data' : data };
    return result;
};

module.exports = {
    setSelect,
    setId,
    setTabla,
    setWhere,
    setWhereOr,
    setJoin,
    setPagination,
    setOrderBy,
    setObject,
    limpiaRegistros,
    genericSelect,
    genericSelectData,
    genericSelectOne,
    genericSelectCount,
    genericInsert,
    genericUpdate,
    genericDelete
};