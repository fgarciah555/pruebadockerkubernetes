## Ejercicio 1 ##

Pasos a seguir

1 - INGRESAR A LA CARPETA ejercicio_1
2 - EJECUTAR EL ARCHIVO docker-compose up
3 - EL SERVICIO DESCARGARA LAS IMAGENES GENERADAS Y EJECUTARA LAS IMAGENES EN EL CONTENEDOR.
4 - INGRESAR A LA URL http://localhost:3000 EL CUAL DESPLEGARA EL FRONT DE LA APP QUE REGISTRA VISITAS POR IP Y FECHA

Descripcion

El archivo docker-compose indica que:

    -   3 imagenes a usar cada una subida a nuestros contenedores (fgarciah555/backhites:1 - fgarciah555/bbddhites:1 - fgarciah555/fronthites:1)
    -   Indica volumen para mysql
    -   Se creo la red "redHites"
    -   El servicio de app depende de "condition: service_healthy" Para el despliegue de los contenedores
    -   Todas las imagenes cuentan con la politica de "restart always"
    -   Cada imagen cuenta con varables de entorno