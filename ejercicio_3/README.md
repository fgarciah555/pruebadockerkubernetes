## Ejercicio 3 ##
Parte 3 - Conceptos. (15%)
Descripción de la sección.
En kubernetes existen varios conceptos además de los vistos en clases, como
también herramientas de terceros que nos ayudan a la gestión y mantenimiento de
manifiestos o del cluster kubernetes en cuestión.
Definir y describir los siguientes conceptos.

● Kustomize
- Herramienta que permite personalizar despliegues sin necesidad de usar plantillas, la cual permite la personalizar archivos YAML dejando los originales sin modificar. El enfoque de Kustomize se denomina Kustomization, se basa en transformar las configuraciones mediante un archivo llamado kuztomization.yaml para poder lograr distintos parámetros de configuración con un archivo base.

● Helm
- Nombre asignado desde el termino marítimo de timón, es una herramienta para la gestión de paquetes de Kubernetes, principalmente definir, instalar, versionar, publicar y actualizar aplicaciones complejas. Helm entrega sus beneficios mediante Helm Charts, que serían las “cartas de navegación” o bien asistente para realizar estos trabajos. Helm es mantenido por CNCF en colaboración con Microsoft, Google, Bitname y su propia comunidad.

● Kompose
- Es huna herramienta que permite levantar aplicaciones en Kubernetes con un solo comando usando la definición del archivo Docker-compose, por lo tanto, sus ventajas. además de generar los archivos deployment y services necesarios para levantar la aplicación. 

● Ingress
- Es una herramienta que se levanta dentro de Kubernetes, un pod, que se ejecutan en el Cluster para la administración de accesos mediante una configuración designada. Permite accesos a servicios desde fuera del Cluster mediante HTTP(S) mediante el conjunto de reglas especificadas, además da una URL Externa a las aplicaciones y se puede generar un balanceador de carga o terminación SSL, básicamente un proxy inverso.

● CertManager
- Herramienta para la administración de certificados TSL de Kubernetes, agrega certificados, y emisores de certificados como recursos a los Cluster de Kubernetes, simplificando el proceso de obtención, renovación y uso de estos. Esta herramienta permite agregar emisores de certificados de distinto tipo, se asegurará de que el certificado esté válido y actualizado e intentará renovarlos antes de que caduquen.

Para cada definición, favor escribir entre 4 a 10 líneas como máximo.