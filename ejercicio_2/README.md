## Ejercicio 2 ##
Entrar en la carpeta con consola de comando donde contenga todos los archivos
Generar namespace:
-   kubectl create -f ./namespaces.yaml
Montar todo:
-   kubectl apply -f redhites-networkpolicy.yaml,dbsecrets.yaml,front-configmap.yaml,front-deployment.yaml,front-service.yaml,mysqldb-deployment.yaml,mysqldb-service.yaml,mysqldb-volume.yaml,app-configmap.yaml,app-deploy.yaml,app-service.yaml,hpa-app.yaml,hpa-front.yaml